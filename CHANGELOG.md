# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete temporary qualifiers for gryc.

## v1.1 (2020-10-02)

### Edited

* Rename all the chromosome with the iGenolevures nomenclature.
* Build the GRYC hierarchy.

## v1.0 (2020-10-02)

### Edited

* Locus CAGL0G07183t: locus_tag confusion, with CAGL0G07166t, CAGL0G07183g and CAGL0G07161t, this is just a single mobile element.

### Added

* The 8 annotated chromosomes of Candida albicans SC 5314 (source EBI, [GCA_000182965.3](https://www.ebi.ac.uk/ena/browser/view/GCA_000182965.3)).
