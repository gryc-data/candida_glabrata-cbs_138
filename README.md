## *Candida* (*Nakaseomyces*) *glabrata* CBS 138

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA13831](https://www.ebi.ac.uk/ena/browser/view/PRJNA13831)
* **Assembly accession**: [GCA_000002545.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000002545.2)
* **Original submitter**: Genolevures consortium

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM254v2
* **Assembly length**: 12,318,245
* **#Chromosomes**: 13
* **Mitochondiral**: No
* **N50 (L50)**: 1,100,349 (5)

### Annotation overview

* **Original annotator**: Genolevures consortium
* **CDS count**: 5202
* **Pseudogene count**: 22
* **tRNA count**: 207
* **rRNA count**: 4
* **Mobile element count**: 2
